package main

import (
	"fmt"
	"math"
)

type Vector struct {
	X, Y float64
}

var Limits Vector = Vector{}

func (v Vector) Add(w Vector) Vector {
	return Vector{v.X + w.X, v.Y + w.Y}
}

func (v Vector) Sub(w Vector) Vector {
	return Vector{v.X - w.X, v.Y - w.Y}
}

func (v Vector) Mult(w float64) Vector {
	return Vector{v.X * w, v.Y * w}
}

func (v Vector) Div(w float64) Vector {
	return Vector{v.X / w, v.Y / w}
}

func (v Vector) Norm() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v Vector) Normalized() Vector {
	return v.Div(v.Norm())
}

func (v Vector) Limit(lim float64) Vector {
	return v.Normalized().Mult(lim)
}

func (v Vector) Wrap(w Vector) Vector {
	// Result should be in [0, W) and not (-W, W)
	return Vector{math.Mod(math.Mod(v.X, w.X)+w.X, w.X),
		math.Mod(math.Mod(v.Y, w.Y)+w.Y, w.Y)}
}

func (v Vector) Distance(w Vector) float64 {
	dx, dy := v.X-w.X, v.Y-w.Y

	switch {
	case dx > Limits.X/2:
		dx = -(dx - Limits.X)
	case dx < -Limits.X/2:
		dx = -(dx + Limits.X)
	}

	switch {
	case dy > Limits.Y/2:
		dy = -(dy - Limits.Y)
	case dy < -Limits.Y/2:
		dy = -(dy + Limits.Y)
	}

	return math.Sqrt(dx*dx + dy*dy)
}

func (v Vector) String() string {
	return fmt.Sprintf("(%v,%v)", v.X, v.Y)
}
