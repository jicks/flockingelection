package main

import (
	//"fmt"
	"math"
	"math/rand"
)

type Boid struct {
	env              *Environment
	n                int
	influence        uint32
	Color            uint32
	Leader           *Boid
	futureLeader     *Boid
	distanceToLeader float64
	position         Vector
	velocity         Vector
	acceleration     Vector
	Channel          chan *Boid
}

const (
	AlignmentInfluence  = 50
	AlignmentFactor     = 1.5
	CohesionInfluence   = 50
	CohesionFactor      = 1
	SeparationInfluence = 25
	SeparationFactor    = 3
	LeaderInfluence     = 100
	LeaderFactor        = 2
)

func NewBoid(env *Environment, n int) *Boid {
	angle := rand.Float64() * math.Pi * 2
	channel := make(chan *Boid, 2*env.NbBoids)
	color := uint32(rand.Int31())
	boid := &Boid{env: env, n: n, Color: color, position: env.Limits.Div(2),
		velocity: Vector{math.Cos(angle), math.Sin(angle)}, Channel: channel}
	boid.Leader = boid
	return boid
}

func (boid *Boid) Compute() {
	//fmt.Printf("[Boid%d] Current leader is Boid%d\n", boid.n, boid.Leader.n)
	surroundings := boid.env.GetSurroundingBoids(boid)
	boid.sendMessages(surroundings)
	boid.computeFollow()
	boid.computeSeparation(surroundings)
	boid.computeAlignment(surroundings)
	boid.computeCohesion(surroundings)
	boid.futureLeader = boid.getRealLeader()
	boid.distanceToLeader = boid.position.Distance(boid.futureLeader.position)
}

func (boid *Boid) Step() {
	boid.Leader = boid.futureLeader
	x, y := (rand.Float64()*2-1)*boid.acceleration.X/10, (rand.Float64()*2-1)*boid.acceleration.Y/10
	boid.acceleration = boid.acceleration.Add(Vector{x, y})
	boid.velocity = boid.velocity.Add(boid.acceleration)
	boid.velocity = boid.velocity.Limit(boid.env.MaxSpeed)
	boid.position = boid.position.Add(boid.velocity)
	boid.position = boid.position.Wrap(boid.env.Limits)
	boid.acceleration = Vector{}
	boid.receiveMessages()
	if boid.distanceToLeader > LeaderInfluence {
		boid.Leader = boid
	}
}

func (boid *Boid) computeFollow() {

	distance := boid.position.Distance(boid.Leader.position)
	if distance > 0 && distance < LeaderInfluence {

		steering := boid.Leader.position.Sub(boid.position)
		// Implement Reynolds: Steering = Desired - Velocity
		steering = steering.Normalized().Mult(boid.env.MaxSpeed)
		steering = steering.Sub(boid.velocity).Limit(boid.env.MaxForce)

		boid.acceleration = boid.acceleration.Add(steering.Mult(LeaderFactor))
	}
}

func (boid *Boid) computeSeparation(surroundings []*Boid) {
	steering := Vector{}
	neighborsCount := 0

	for _, neighbor := range surroundings {
		distance := boid.position.Distance(neighbor.position)
		if distance > 0 && distance < SeparationInfluence {
			neighborsCount++
			steeringDiff := boid.position.Sub(neighbor.position).Normalized()
			steering = steering.Add(steeringDiff.Div(distance))
		}
	}

	if neighborsCount > 0 {
		steering = steering.Div(float64(neighborsCount))
	}

	if steering.Norm() > 0 {
		// Implement Reynolds: Steering = Desired - Velocity
		steering = steering.Normalized().Mult(boid.env.MaxSpeed)
		steering = steering.Sub(boid.velocity).Limit(boid.env.MaxForce)

		boid.acceleration = boid.acceleration.Add(steering.Mult(SeparationFactor))
	}
}

func (boid *Boid) computeAlignment(surroundings []*Boid) {
	steering := Vector{}
	neighborsCount := 0

	for _, neighbor := range surroundings {
		distance := boid.position.Distance(neighbor.position)
		if distance > 0 && distance < AlignmentInfluence {
			neighborsCount++
			steering = steering.Add(neighbor.velocity)
		}
	}

	if neighborsCount > 0 {
		steering = steering.Div(float64(neighborsCount))
		// Implement Reynolds: Steering = Desired - Velocity
		steering = steering.Normalized().Mult(boid.env.MaxSpeed)
		steering = steering.Sub(boid.velocity).Limit(boid.env.MaxForce)

		boid.acceleration = boid.acceleration.Add(steering.Mult(AlignmentFactor))
	}
}

func (boid *Boid) computeCohesion(surroundings []*Boid) {
	steering := Vector{}
	neighborsCount := 0

	for _, neighbor := range surroundings {
		distance := boid.position.Distance(neighbor.position)
		if distance > 0 && distance < CohesionInfluence {
			neighborsCount++
			steering = steering.Add(neighbor.position)
		}
	}

	if neighborsCount > 0 {
		steering = steering.Div(float64(neighborsCount)).Sub(boid.position)
		// Implement Reynolds: Steering = Desired - Velocity
		steering = steering.Normalized().Mult(boid.env.MaxSpeed)
		steering = steering.Sub(boid.velocity).Limit(boid.env.MaxForce)

		boid.acceleration = boid.acceleration.Add(steering.Mult(CohesionFactor))
	}
}

func (boid *Boid) GetSpritePolygon() []Vector {
	point1 := boid.position.Add(boid.velocity.Normalized().Mult(8))
	point2 := Vector{-boid.velocity.Y, boid.velocity.X}.Normalized().Mult(5)
	point2 = boid.position.Add(point2)
	point3 := Vector{-boid.velocity.Y, boid.velocity.X}.Normalized().Mult(5)
	point3 = boid.position.Sub(point3)
	return []Vector{point1, point2, point3}
}

func (boid *Boid) updateInfluence() {
	boid.influence = 0
	for _, neighbor := range boid.env.GetSurroundingBoids(boid) {
		if boid.position.Distance(neighbor.position) < LeaderInfluence {
			boid.influence++
		}
	}
}

func (boid *Boid) getRealLeader() *Boid {
	leader := boid.Leader
	for leader != leader.Leader {
		leader = leader.Leader
	}
	return leader
}

func (boid *Boid) sendMessages(surroundings []*Boid) {
	if boid.Leader != boid {
		return
	}

	boid.updateInfluence()
	boidsGroup := make(map[*Boid]bool)
	for _, neighbor := range surroundings {
		distance := boid.position.Distance(neighbor.position)
		realLeader := neighbor.getRealLeader()
		_, ok := boidsGroup[realLeader]
		if distance < LeaderInfluence && !ok && realLeader != boid {
			//fmt.Printf("[Boid%d] Sending message to Boid%d\n", boid.n, realLeader.n)
			boidsGroup[realLeader] = true
			realLeader.Channel <- boid
		}
	}
}

func (boid *Boid) receiveMessages() {
Loop:
	for {
		select {
		case neighbor := <-boid.Channel:
			//fmt.Printf("[Boid%d] Received message from Boid%d\n", boid.n, neighbor.n)
			if boid.Leader == boid && neighbor.influence > boid.influence {
				boid.Leader = neighbor
			}
		default:
			break Loop
		}
	}
}
