package main

import (
	"github.com/neagix/Go-SDL/sdl"
	"math"
	"unsafe"
)

type Surface struct {
	sdl.Surface
}

func abs(a int32) int32 {
	if a > 0 {
		return a
	}
	return -a
}

func (s *Surface) DrawPixel(x, y int32, color uint32) {
	if x >= s.W || x < 0 || y >= s.H || y <= 0 {
		return
	}
	pixels := uintptr(s.Pixels)
	pixels += (uintptr)((y*s.W)+x) * unsafe.Sizeof(color)
	pp := (*uint32)(unsafe.Pointer(pixels))
	*pp = color
}

// Using Xiaolin Wu's line algorithm
func ipart(x float64) float64 {
	return math.Floor(x)
}

func round(x float64) float64 {
	return ipart(x + .5)
}

func fpart(x float64) float64 {
	return x - ipart(x)
}

func rfpart(x float64) float64 {
	return 1 - fpart(x)
}

// Plots anti-aliased line by Xiaolin Wu's line algorithm.
func (s *Surface) DrawLine(x1, y1, x2, y2 float64, color uint32) {
	// straight translation of WP pseudocode
	dx := x2 - x1
	dy := y2 - y1
	ax := dx
	if ax < 0 {
		ax = -ax
	}
	ay := dy
	if ay < 0 {
		ay = -ay
	}
	// plot function set here to handle the two cases of slope
	var plot func(int32, int32, uint32)
	if ax < ay {
		x1, y1 = y1, x1
		x2, y2 = y2, x2
		dx, dy = dy, dx
		plot = func(x, y int32, c uint32) {
			s.DrawPixel(y, x, c)
		}
	} else {
		plot = func(x, y int32, c uint32) {
			s.DrawPixel(x, y, c)
		}
	}
	if x2 < x1 {
		x1, x2 = x2, x1
		y1, y2 = y2, y1
	}
	gradient := dy / dx

	// handle first endpoint
	xend := round(x1)
	yend := y1 + gradient*(xend-x1)
	xpxl1 := int32(xend) // this will be used in the main loop
	ypxl1 := int32(ipart(yend))
	plot(xpxl1, ypxl1, color)
	plot(xpxl1, ypxl1+1, color)
	intery := yend + gradient // first y-intersection for the main loop

	// handle second endpoint
	xend = round(x2)
	yend = y2 + gradient*(xend-x2)
	xpxl2 := int32(xend) // this will be used in the main loop
	ypxl2 := int32(ipart(yend))
	plot(xpxl2, ypxl2, color)
	plot(xpxl2, ypxl2+1, color)

	// main loop
	for x := xpxl1 + 1; x <= xpxl2-1; x++ {
		plot(x, int32(ipart(intery)), color)
		plot(x, int32(ipart(intery))+1, color)
		intery = intery + gradient
	}
}

// Using scanline algorithm
func (s *Surface) FillPolygon(corners []Vector, color uint32) {
	for i := 0; i < len(corners)-1; i++ {
		s.DrawLine(corners[i].X, corners[i].Y,
			corners[i+1].X, corners[i+1].Y, color)
	}
	if size := len(corners) - 1; size > 0 {
		s.DrawLine(corners[size].X, corners[size].Y,
			corners[0].X, corners[0].Y, color)
	}
	// ACTUALLY IMPLEMENT SCANLINE !!
}
