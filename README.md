flockingelection
================

`flockingelection` is a program intended to play with concurrency features in
Go.

The main idea was to implement two different things:

- a flocking algorithm, based on Craig Reynolds' simulation program
- a dynamic leader election algorithm

This second algorithm is voluntarily simple and was an excuse to play with
channel and multiplexing in Go.


Many thanks to neagix for his (and 0xe2-0x9a-0x9b's) Go-SDL bindings!
