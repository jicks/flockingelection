package main

type Environment struct {
	boids    []*Boid
	NbBoids  int
	Limits   Vector
	MaxSpeed float64
	MaxForce float64
}

func (env *Environment) AddBoid(boid *Boid) {
	env.boids = append(env.boids, boid)
}

func (env *Environment) GetSurroundingBoids(boid *Boid) []*Boid {
	return env.boids
}
