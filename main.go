package main

import (
	"fmt"
	"github.com/neagix/Go-SDL/sdl"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"
)

type Display struct {
	Polygon []Vector
	Color   uint32
}

func runBoid(boid *Boid, n int, displayChannel chan Display,
	synch chan bool, wg *sync.WaitGroup) {
	for {
		// We compute the move we will do, without fear of interfering with
		// others
		boid.Compute()
		wg.Done()
		wg.Wait()
		// We actually move here, nothing else
		boid.Step()
		displayChannel <- Display{boid.GetSpritePolygon(), boid.Leader.Color}
		<-synch
	}
}

func runDisplay(nbBoids int, displayChannel chan Display, synch chan bool, wg *sync.WaitGroup) {
	if sdl.Init(sdl.INIT_VIDEO) != 0 {
		panic(sdl.GetError())
	}

	defer sdl.Quit()

	screen := &Surface{*sdl.SetVideoMode(int(Limits.X), int(Limits.Y), 32, 0)}
	if screen == nil {
		panic(sdl.GetError())
	}

	sdl.WM_SetCaption("Flocking Election", "")
	ticker := time.NewTicker(1e9 / 80 /*80 Hz*/)
	screen.FillRect(nil, 0)
	screen.Flip()

loop:
	for {
		select {
		case <-ticker.C:
			screen.FillRect(nil, 0)
			for i := 0; i < nbBoids; i++ {
				disp := <-displayChannel
				screen.FillPolygon(disp.Polygon, disp.Color)
			}
			wg.Add(nbBoids)
			for i := 0; i < nbBoids; i++ {
				synch <- true
			}
			screen.Flip()

		case event := <-sdl.Events:
			switch event.(type) {
			case sdl.QuitEvent:
				break loop
			}
		}
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintln(os.Stderr, "Usage: ", os.Args[0], " <number of boids> <number of core to use>")
		os.Exit(1)
	}
	nbBoids, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error First argument should be an int.")
		fmt.Fprintln(os.Stderr, "Usage: ", os.Args[0], " <number of boids> <number of core to use>")
		os.Exit(1)
	}
	nbThreads, err := strconv.Atoi(os.Args[2])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error: Second argument should be an int.")
		fmt.Fprintln(os.Stderr, "Usage: ", os.Args[0], " <number of boids> <number of core to use>")
		os.Exit(1)
	}
	if maxThreads := runtime.NumCPU(); nbThreads > maxThreads {
		nbThreads = maxThreads
	}
	runtime.GOMAXPROCS(nbThreads)

	Limits = Vector{1280, 720}
	boids := make([]*Boid, nbBoids)
	displayChannel, synch := make(chan Display, nbBoids), make(chan bool, nbBoids)
	env := Environment{NbBoids: nbBoids, Limits: Limits, MaxSpeed: 2.0, MaxForce: 0.03}
	wg := sync.WaitGroup{}
	wg.Add(nbBoids)

	for i := 0; i < nbBoids; i++ {
		boids[i] = NewBoid(&env, i)
		env.AddBoid(boids[i])
	}

	for i := 0; i < nbBoids; i++ {
		go runBoid(boids[i], i, displayChannel, synch, &wg)
	}

	runDisplay(nbBoids, displayChannel, synch, &wg)
}
